# Webpack 4 Boilerplate

## Features

* Example assets directory for reference and **demo** building of:
  * *JavaScript*
  * *SASS*
  * *Images*
  * *Fonts*
* Support for **assets optimization** for production:
  * **Minification** of *JavaScript* and *CSS* files.
  * **Inline** **images** / **fonts** files having file size below a *configurable* threshold value.
* Code style and formatting **linters** configuration and setup for:
  * *SASS*
  * *JavaScript*
* Latest [Webpack 4](https://github.com/webpack/webpack) - *JavaScript* module bundler.
* Latest [SASS/CSS](https://github.com/sass/node-sass) compiler based on `node-sass` which provides bindings to `libsass`.
* Latest [Babel 7](https://github.com/babel/babel) (`@babel/core`) - JavaScript compiler - _Use next generation JavaScript, today._
* Configured and ready to use **BrowserSync** plugin - `browser-sync-webpack-plugin`

## Requirements

* `node` **>=** `10.13.0`
* `npm`

# Setup

## Installation

1. Choose and download the template from [List of Releases](https://github.com/WeAreAthlon/frontend-webpack-boilerplate/releases).
2. Extract the release archive to a new directory, rename it to your project name and browse the directory.
3. Install all dependencies using `npm` clean install. 

```sh 
$ npm install
```

* Configure `webpack.config.js`

## Development / Build Assets

### Assets Source

* _SASS_ files are located under `/src/scss/`
* _JavaScript_ files with support of _ES6 / ECMAScript 2016(ES7)_ files are located under `/src/js/`
* _Image_ files are located under `/src/images/`
* _Font_ files are located under `/src/fonts/`
* _HTML_ files are located under `/src/`

### Build Assets

```sh
$ npm run build
```

### Enable Source Files Watcher

```sh
$ npm run watch
```

```js
module.exports = {
  //...
  watchOptions: {
    poll: 1000 // Check for changes every second
  }
};
```

## Production / Build Assets

* Optimize assets for production by:

```sh
$ npm run production
```

## Run Code Style Linters

* **SASS**

```sh
$ npm run lint-sass
```
* **JS**

```sh
$ npm run lint-js
```


### Bundle

Executes both `install` and `watch` tasks in a single command convenient for development:

```sh
$ npm run bundle
```